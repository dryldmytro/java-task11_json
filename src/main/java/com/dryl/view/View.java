package com.dryl.view;

import com.dryl.controller.Controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.dryl.Constant.*;

public class View {

    private Controller controller;
    Map<String, String> map;

    public View() {
        controller = new Controller();
        map = new LinkedHashMap<>();
        map.put("1", bundle.getString("task1"));
        map.put("2", bundle.getString("task2"));
        map.put("3", bundle.getString("task3"));
        map.put("4", bundle.getString("task4"));
        map.put("5", bundle.getString("task5"));
        map.put("Q", bundle.getString("Q"));
    }

    public void outputMenu() throws IOException {
        System.out.println("\nMENU:");
        for (String str : map.values()) {
            logger.trace(str);
        }
        logger.trace(bundle.getString("choice"));
        String userChoice = scanner.nextLine().toUpperCase();
        switch (userChoice) {
            case "1":
                controller.getTask1();
                outputMenu();
            case "2":
                controller.getTask2();
                outputMenu();
            case "3":
                controller.getTask3();
                outputMenu();
            case "4":
                controller.getTask4();
                outputMenu();
                break;
            case "5":
                controller.getTask5();
                outputMenu();
            case "Q":
                System.exit(0);
            default:
                logger.trace(bundle.getString("bad_choice"));
                outputMenu();
        }
    }
}
