package com.dryl.model.parser.gson;

import com.dryl.model.Vouchers;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileReader;
import java.io.FileWriter;

import static com.dryl.Constant.bundle;

public class MyJSONParser {
    public static Vouchers parse(FileReader json){
        Gson gson = new Gson();
        return gson.fromJson(json,Vouchers.class);
    }
    public static void createJson(Vouchers vouchers){
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try (FileWriter fileWriter = new FileWriter(bundle.getString("gsonparse"))){
            gson.toJson(vouchers,fileWriter);
        }catch (Exception e){}

    }
}
