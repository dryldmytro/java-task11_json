package com.dryl.model.parser.jackson;

import com.dryl.model.Vouchers;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.File;
import java.io.IOException;

import static com.dryl.Constant.bundle;

public class MyJacksonParser {
    public static Vouchers parse(File file) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Vouchers v = mapper.readValue(file, Vouchers.class);
        return v;
    }

    public static void parseInJson(Vouchers vouchers) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.writerWithDefaultPrettyPrinter().writeValue(new File(bundle.getString("jacksonparse")), vouchers);
    }
}
