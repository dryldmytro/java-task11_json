package com.dryl.model;

public class Hotel {
    private int stars;
    private String food;
    private String room;
    private boolean wifi;
    private boolean tv;

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public boolean isWifi() {
        return wifi;
    }

    public void setWifi(boolean wifi) {
        this.wifi = wifi;
    }

    public boolean isTv() {
        return tv;
    }

    public void setTv(boolean tv) {
        this.tv = tv;
    }

    @Override
    public String toString() {
        return "Hotel{" +
                "stars=" + stars +
                ", food='" + food + '\'' +
                ", room='" + room + '\'' +
                ", wifi=" + wifi +
                ", tv=" + tv +
                '}';
    }
}
