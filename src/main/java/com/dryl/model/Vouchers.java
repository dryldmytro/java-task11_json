package com.dryl.model;

import java.util.List;

public class Vouchers {
    private List<Voucher>vouchers;

    public List<Voucher> getVouchers() {
        return vouchers;
    }

    public void setVouchers(List<Voucher> vouchers) {
        this.vouchers = vouchers;
    }
}
