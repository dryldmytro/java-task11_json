package com.dryl.model;

import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.FileNotFoundException;
import java.io.FileReader;

import static com.dryl.Constant.bundle;
import static com.dryl.Constant.logger;

public class SchemaJ {
    public static void verifyJSON(FileReader voucher,FileReader voucherSchema) {
        try {
            JSONObject schemaJson = new JSONObject(
                    new JSONTokener(
                            voucherSchema));
            JSONObject subject = new JSONObject(
                    new JSONTokener(voucher));
            Schema schema = SchemaLoader.load(schemaJson);
            schema.validate(subject);
            logger.info(bundle.getString("good"));
        }catch (Exception e){
            logger.error(bundle.getString("bad"));
        }
    }
}
