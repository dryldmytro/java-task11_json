package com.dryl.controller;

import com.dryl.model.SchemaJ;
import com.dryl.model.Voucher;
import com.dryl.model.VoucherComparator;
import com.dryl.model.Vouchers;
import com.dryl.model.parser.gson.MyJSONParser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static com.dryl.Constant.bundle;
import static com.dryl.Constant.logger;
import static com.dryl.model.parser.jackson.MyJacksonParser.parse;
import static com.dryl.model.parser.jackson.MyJacksonParser.parseInJson;

public class Controller {
    public void getTask1() throws FileNotFoundException {
        FileReader voucher = new FileReader(bundle.getString("voucher"));
        Vouchers v = MyJSONParser.parse(voucher);
        List<Voucher> list = v.getVouchers();
        list.forEach(x -> logger.info(x));

    }

    public void getTask2() throws FileNotFoundException {
        FileReader voucher = new FileReader(bundle.getString("voucher"));
        Vouchers v = MyJSONParser.parse(voucher);
        MyJSONParser.createJson(v);
    }

    public void getTask3() throws IOException {
        File file = new File(bundle.getString("voucher"));
        Vouchers v = parse(file);
        List<Voucher> list = v.getVouchers();
        Collections.sort(list, new VoucherComparator());
        list.forEach(x -> logger.info(x));
    }

    public void getTask4() throws IOException {
        File file = new File(bundle.getString("voucher"));
        Vouchers v = parse(file);
        parseInJson(v);
    }
    public void getTask5() throws FileNotFoundException {
        FileReader voucher = new FileReader(bundle.getString("voucher"));
        FileReader schema = new FileReader(bundle.getString("voucherSchema"));
        SchemaJ.verifyJSON(voucher,schema);
    }
}
